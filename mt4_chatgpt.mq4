#property copyright ""
#property link      ""
#property version   "1.04"
#property strict
#property indicator_chart_window

#define OPEN_TYPE_PRECONFIG                0           // use the configuration by default
#define FLAG_KEEP_CONNECTION               0x00400000  // do not terminate the connection
#define FLAG_PRAGMA_NOCACHE                0x00000100  // no cashing of the page
#define FLAG_RELOAD                        0x80000000  // receive the page from the server when accessing it
#define SERVICE_HTTP                       3           // the required protocol
#define FLAG_SECURE                        0x00800000  // use PCT/SSL if applicable (HTTP)
#define DEFAULT_HTTPS_PORT                 443

#import "wininet.dll"
int InternetOpenW(string sAgent, int lAccessType, string sProxyName, string sProxyBypass, int lFlags);
int InternetConnectW(int hInternet, string ServerName, int nServerPort, string lpszUsername, string lpszPassword, int dwService, int dwFlags, int dwContext);
int HttpOpenRequestW(int hConnect, string Verb, string ObjectName, string Version, string Referer, string AcceptTypes, int dwFlags, int dwContext);
int HttpSendRequestW(int hRequest, string &lpszHeaders, int dwHeadersLength, uchar &lpOptional[], int dwOptionalLength);
int InternetReadFile(int hFile, uchar &sBuffer[], int lNumBytesToRead, int &lNumberOfBytesRead);
int InternetCloseHandle(int hInet);
#import

extern string ChatGPT_API_Key = "";             //chatGPT API キー
extern string ChatGPT_Model = "gpt-3.5-turbo";  //chatGPT API モデル

string command_message = "こんにちは";
string edit_obj_nanme = "chatgptprompt";
string button_prompt_name = "chatgptprompt_button";


void OnDeinit(const int reason)
{
  ObjectDelete(edit_obj_nanme);
  ObjectDelete(button_prompt_name);
}


int OnInit()
{
  if (MQLInfoInteger(MQL_DLLS_ALLOWED) != 1) {
    Print("==================");
    Print("DLL is not allowed");
    Print("==================");
    return(INIT_FAILED);
  }

  EditCreate(0, edit_obj_nanme, 0, 3, 53, 650, 50, "こんにちは", "MSゴシック", 10,  ALIGN_LEFT, false, CORNER_LEFT_LOWER, clrBlack, clrWhite, clrNONE, false, false, false, false, 0);
  ButtonCreate(0, button_prompt_name, 0, 655, 52, 80, 48, CORNER_LEFT_LOWER, "Send", "MSゴシック", 18, clrBlack, clrRed, clrNONE, false, false, false, false, 0);
  return(INIT_SUCCEEDED);
}


int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
  return(rates_total);
}


void OnChartEvent(
                 const int     id,
                 const long&   lparam,
                 const double& dparam,
                 const string& sparam)
{
  if(id == CHARTEVENT_OBJECT_CLICK){
    command_message = ObjectGetString(0, edit_obj_nanme, OBJPROP_TEXT, 0);
    if(sparam == button_prompt_name){
      string return_text = Connect2ChatGPT(command_message);
      return_text = GetMessage(return_text);
      Print("command_message : ", command_message);
      Print(return_text);
      Comment(return_text);
    }

  }
}


string Connect2ChatGPT(string message){
  string result = "";
  int Session = InternetOpenW("MetaTrader 4 Terminal", 0, "", "", 0);
  if(Session<=0){
    Print("-Err create Session");
  }

  string nill = "";
  int connect = InternetConnectW(Session, "api.openai.com", DEFAULT_HTTPS_PORT, nill, nill, SERVICE_HTTP, 0, 0);
  if(connect<=0){
    Print("-Err create Connect");
    if(Session>0) {
      InternetCloseHandle(Session); Session=-1;
    }
  }

  int hRequest = HttpOpenRequestW(connect, "POST", "/v1/chat/completions", "HTTP/1.1", NULL, NULL, (int)(FLAG_SECURE|FLAG_KEEP_CONNECTION|FLAG_RELOAD|FLAG_PRAGMA_NOCACHE), 0);
  if(hRequest<=0){
    Print("-Err OpenRequest");
    InternetCloseHandle(connect);
  }

  uchar data[];
  string text;
  string headers = "Authorization: Bearer " + ChatGPT_API_Key + "\r\n " + "Content-Type: application/json\r\n";
  string dataString = "{ \"model\": \"" + ChatGPT_Model + "\", \"messages\": [{\"role\": \"user\", \"content\": \"" + message +"\"}] }";
  StringToCharArray(dataString, data, 0, -1, CP_UTF8);
  int hSend = HttpSendRequestW(hRequest, headers, StringLen(headers), data, ArraySize(data)-1);
  if(hSend <= 0){
    Print("Err SendRequest");
    if(connect > 0) InternetCloseHandle(hRequest);
    if(Session > 0) InternetCloseHandle(Session);  Session  = -1;
    if(connect > 0) InternetCloseHandle(connect);  connect  = -1;
  }else{
    uchar ch[4096];
    int dwBytes;
    while(InternetReadFile(hRequest, ch, 4095, dwBytes)){
      if(dwBytes <= 0) break;
      text += CharArrayToString(ch, 0, dwBytes, CP_UTF8);
    }
  }

  InternetCloseHandle(hSend);
  InternetCloseHandle(hRequest);
  if(Session > 0) InternetCloseHandle(Session); Session = -1;
  if(connect > 0) InternetCloseHandle(connect); connect = -1;
  result = text;
  return result;
}

string GetMessage(string text){
  //----------------------------------文字列パースの仕組み---------------------------------------
  //
  //----------------最初の文章----------------
  //0 : {"id":"chatcmpl-72Z8gIO9WTCSrMPGaPuVDNEaA2rH8","object":"chat.completion","created":1680846086,"model":"gpt-3.5-turbo-0301","usage":{"prompt_tokens":9,"completion_tokens":57,"total_tokens":66},"choices":[{"message":{"role":"assistant","content":"こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。"},"finish_reason":"stop","index":0}]}
  //
  //----------------「{」文字区切りで分けた5番目配列文字列を取得----------------
  //"role":"assistant","content":"こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。"},"finish_reason":"stop","index":0}]}
  //
  //----------------「:」文字区切りで分けた3番目以降の配列の文字列を取得----------------　　
  //"こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。"},"finish_reason"
  //
  //----------------「}」文字区切りで分けた1番目配列文字列を取得----------------
  //"こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。"
  //
  //----------------先頭文字列「"」ダブルクォーテーションを削除----------------
  //こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。"
  //
  //----------------文字列末尾の「"」ダブルクォーテーションを削除----------------  
  //こんにちは！いい天気ですね。何かお困りのことはありますか？私はAIのアシスタントですので、お手伝いできることがあればお知らせください。
  //
  //--------------------------------------------------------------------------------------------

  string message;
  string result1[];
  string result2[];
  string result3[];
  int sep_num=0;
  ushort u_sep = StringGetCharacter("{", 0);
  if(StringSubstr(text, 0, 1)=="{"){
    sep_num = StringSplit(text, u_sep, result1);
    if(sep_num>=5){
      message = result1[4];
    }
  }

  u_sep = StringGetCharacter(":", 0);
  sep_num = StringSplit(message, u_sep, result2);
  if(sep_num>=3){
    message = "";
    for(int i=2; i<=sep_num-1; i++){
      message += result2[i];
    }
  }

  u_sep = StringGetCharacter("}", 0);
  sep_num = StringSplit(message, u_sep, result3);
  if(sep_num>=1){
    message = result3[0];
  }

  if(StringSubstr(message, 0, 1)=="\""){
    message = StringSubstr(message, 1, -1);
  }

  int stringlength = StringLen(message);
  if(StringSubstr(message, stringlength-1 , 1)=="\""){
    message = StringSubstr(message, 0, stringlength-1);
  }
  return message;
}

bool EditCreate(const long             chart_ID=0,               // chart's ID
                const string           name="Edit",              // object name
                const int              sub_window=0,             // subwindow index
                const int              x=0,                      // X coordinate
                const int              y=0,                      // Y coordinate
                const int              width=50,                 // width
                const int              height=18,                // height
                const string           text="Text",              // text
                const string           font="Arial",             // font
                const int              font_size=10,             // font size
                const ENUM_ALIGN_MODE  align=ALIGN_CENTER,       // alignment type
                const bool             read_only=false,          // ability to edit
                const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                const color            clr=clrBlack,             // text color
                const color            back_clr=clrWhite,        // background color
                const color            border_clr=clrNONE,       // border color
                const bool             back=false,               // in the background
                const bool             selectable=false,         // highlight to move
                const bool             selected=false,           // highlight to move
                const bool             hidden=true,              // hidden in the object list
                const long             z_order=0)                // priority for mouse click
{
  ResetLastError();
  if(!ObjectCreate(chart_ID,name,OBJ_EDIT,sub_window,0,0)){
    Print(__FUNCTION__, ": failed to create \"Edit\" object! Error code = ",GetLastError());
    return(false);
  }

  ObjectSetInteger(chart_ID, name,OBJPROP_XDISTANCE, x);
  ObjectSetInteger(chart_ID, name,OBJPROP_YDISTANCE, y);
  ObjectSetInteger(chart_ID, name,OBJPROP_XSIZE, width);
  ObjectSetInteger(chart_ID, name,OBJPROP_YSIZE, height);
  ObjectSetString(chart_ID, name,OBJPROP_TEXT, text);
  ObjectSetString(chart_ID, name,OBJPROP_FONT, font);
  ObjectSetInteger(chart_ID, name,OBJPROP_FONTSIZE, font_size);
  ObjectSetInteger(chart_ID, name,OBJPROP_ALIGN, align);
  ObjectSetInteger(chart_ID, name,OBJPROP_READONLY, read_only);
  ObjectSetInteger(chart_ID, name,OBJPROP_CORNER, corner);
  ObjectSetInteger(chart_ID, name,OBJPROP_COLOR, clr);
  ObjectSetInteger(chart_ID, name,OBJPROP_BGCOLOR, back_clr);
  ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR, border_clr);
  ObjectSetInteger(chart_ID,name,OBJPROP_BACK, back);
  ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE, selectable);
  ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED, selected);
  ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN, hidden);
  ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER, z_order);
  return(true);
}

bool ButtonCreate(const long              chart_ID=0,               // chart's ID
                  const string            name="Button",            // button name
                  const int               sub_window=0,             // subwindow index
                  const int               x=0,                      // X coordinate
                  const int               y=0,                      // Y coordinate
                  const int               width=50,                 // button width
                  const int               height=18,                // button height
                  const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                  const string            text="Button",            // text
                  const string            font="Arial",             // font
                  const int               font_size=10,             // font size
                  const color             clr=clrBlack,             // text color
                  const color             back_clr=C'236,233,216',  // background color
                  const color             border_clr=clrNONE,       // border color
                  const bool              state=false,              // pressed/released
                  const bool              back=false,               // in the background
                  const bool              selectable=false,          // highlight to move
                  const bool              selected=false,          // highlight to move
                  const bool              hidden=true,              // hidden in the object list
                  const long              z_order=0)                // priority for mouse click
{
  ResetLastError();

  if(!ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0)){
    Print(__FUNCTION__, ": failed to create the button! Error code = ",GetLastError());
    return(false);
  }

  ObjectSetInteger(chart_ID, name, OBJPROP_XDISTANCE, x);
  ObjectSetInteger(chart_ID, name, OBJPROP_YDISTANCE, y);
  ObjectSetInteger(chart_ID, name, OBJPROP_XSIZE, width);
  ObjectSetInteger(chart_ID, name, OBJPROP_YSIZE, height);
  ObjectSetInteger(chart_ID, name, OBJPROP_CORNER, corner);
  ObjectSetString(chart_ID, name, OBJPROP_TEXT, text);
  ObjectSetString(chart_ID, name, OBJPROP_FONT, font);
  ObjectSetInteger(chart_ID, name, OBJPROP_FONTSIZE, font_size);
  ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
  ObjectSetInteger(chart_ID, name, OBJPROP_BGCOLOR, back_clr);
  ObjectSetInteger(chart_ID, name, OBJPROP_BORDER_COLOR, border_clr);
  ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);
  ObjectSetInteger(chart_ID, name, OBJPROP_STATE, state);
  ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, selectable);
  ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selected);
  ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
  ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);

  return(true);
}
